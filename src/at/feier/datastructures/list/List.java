package at.feier.datastructures.list;

public class List {
	private Node root;

	public List() {
		root = null;
	}

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node value) {
		this.root = value;
	}

	public void add(Object value) {
		if (root == null) {
			root = new Node(value);
			root.setPrevious(null);
			root.setNext(null);
		} else {
			Node current = root;
			Node previous = null;
			while (current.getNext() != null) {
				previous = current;
				current = current.getNext();
			}
			Node toAdd = new Node(value);
			current.setNext(toAdd);
			current.setPrevious(previous);
			current.getNext().setPrevious(current);
		}
	}

	public void add(Object value, int position) {
		Node current = root;
		Node previous = null;
		for (int i = 0; i < position; i++) {
			previous = current;
			current = current.getNext();
		}
		Node toAdd = new Node(value);
		toAdd.setNext(current);
		toAdd.setPrevious(previous);
		current.setPrevious(toAdd);
		previous.setNext(toAdd);
	}

	public Node get(int index) {
		Node current = root;
		for (int i = 0; i < index; i++) {
			current = current.getNext();
		}
		return current;
	}

	public Node next(int n) {
		Node current = root;
		for (int i = 0; i < n; i++) {
			current = current.getNext();
		}
		return current.getNext();
	}

	public Node prev(int n) {
		Node current = root;
		for (int i = 0; i < n; i++) {
			current = current.getNext();
		}
		return current.getPrevious();
	}
	
	

}
