package at.feier.algorithms.sorting.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BallSorterTest.class, BubbleSortTest.class, InsertionSortTest.class, SelectionSortTest.class })
public class AllTests {

}
