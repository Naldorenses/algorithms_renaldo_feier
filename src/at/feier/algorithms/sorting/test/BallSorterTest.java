package at.feier.algorithms.sorting.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.feier.algorithms.sortalgorithms.BallSorter;
import at.feier.algorithms.sortalgorithms.InsertionSort;

public class BallSorterTest {

	private int[] data = new int[3];
	
	
	@Before
	public void setup(){
		data[0] = 42;
		data[1] = 22;
		data[2] = 22;
	}
	
	@Test
	public void test() {
		int[] clonedata = data.clone();
		BallSorter is = new  BallSorter();
		int[] res = is.sort(clonedata);
		
		assertTrue(isSorted(res));
	}
	
	public boolean isSorted(int[] sorted){
		int old= sorted[0];
		for(int i = 1; i < sorted.length; i++){
			if(old <= sorted[i]){
				old = sorted[i];
			}
			else{
				return false;
			}
		}
		return true;
	}
}
