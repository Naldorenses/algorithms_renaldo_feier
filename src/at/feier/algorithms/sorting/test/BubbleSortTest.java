package at.feier.algorithms.sorting.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.feier.algorithms.sortalgorithms.BubbleSort;
import at.feier.algorithms.sortalgorithms.InsertionSort;

public class BubbleSortTest {

	private int[] data = new int[3];
	
	
	@Before
	public void setup(){
		data[0] = 42;
		data[1] = 12;
		data[2] = 22;
	}
	
	@Test
	public void test() {
		int[] clonedata = data.clone();
		BubbleSort is = new  BubbleSort();
		int[] res = is.sort(clonedata);
		
		assertTrue(isSorted(res));
	}
	
	public boolean isSorted(int[] sorted){
		int old= sorted[0];
		for(int i = 1; i < sorted.length; i++){
			if(old <= sorted[i]){
				old = sorted[i];
			}
			else{
				return false;
			}
		}
		return true;
	}
}
