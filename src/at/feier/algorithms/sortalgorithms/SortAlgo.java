package at.feier.algorithms.sortalgorithms;

public interface SortAlgo {
public int[] sort(int[] data);
public String getName();
}
