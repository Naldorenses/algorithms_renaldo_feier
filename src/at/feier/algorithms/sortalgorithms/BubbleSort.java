package at.feier.algorithms.sortalgorithms;

public class BubbleSort implements SortAlgo {

	@Override
	public int[] sort(int[] input) {
		for (int i = 0; i < input.length; i++) {
			int count = 0;
			if ((i + 1) < input.length) {
				for (int j = 1; j < input.length; j++) {
					if (input[count] > input[j]) {
						int temp = input[j];
						input[j] = input[count];
						input[count] = temp;
						count++;
					} else {
						count++;
					}
				}
			}

		}
		return input;
	}

	@Override
	public String getName() {
		return "BubbleSort";
	}

}
