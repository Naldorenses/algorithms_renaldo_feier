package at.feier.algorithms.sortalgorithms;

import algorithms.MainClass;
import at.feier.algorithms.helper.DataGenerator;

public class SelectionSort implements SortAlgo {

	@Override
	public int[] sort(int[] input) {
		DataGenerator.printData(input);
		int minimum = MainClass.min;
		int size = 0;
		while (minimum != MainClass.max -1) {
			for (int z = input.length - 1; z >= size; z--) {
				if (input[z] <= minimum) {
					int temp = input[size];
					input[size] = input[z];
					input[z] = temp;
					size++;
				}

			}
			minimum++;
		}
		
		for (int q = 0; q < input.length -1; q++) {
			if (input[q] > input[q + 1]) {
				int temp1 = input[q];
				input[q] = input[q + 1];
				input[q + 1] = temp1;
			}
		}
		return input;
	}

	@Override
	public String getName() {
		return "SelectionSort";
	}

}
