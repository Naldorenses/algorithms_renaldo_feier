package at.feier.algorithms.sortalgorithms;

public class InsertionSort implements SortAlgo {

	@Override
	public int[] sort(int[] input) {
		if (input[0] > input[1]) {
			int temp = input[0];
			input[0] = input[1];
			input[1] = temp;
		}
		for (int i = 1; i < input.length - 1; i++) {
			if (input[i] > input[i + 1]) {
				int temp = input[i];
				input[i] = input[i + 1];
				input[i + 1] = temp;
				if (input[i - 1] > input[i]) {
					for (int u = 0 + i; u > 0; u--) {
						if (input[u] < input[u - 1]) {
							int temp1 = input[u];
							input[u] = input[u - 1];
							input[u - 1] = temp1;
						}
					}
				}
			}
		}
		return input;
	}

	@Override
	public String getName() {
		return "InsertionSort";
	}

}
