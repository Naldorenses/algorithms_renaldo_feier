package at.feier.algorithms.sortalgorithms;

public class ShellSort implements SortAlgo{

	@Override
	public int[] sort(int[] data) {
		int inner, outer;
	    int temp;
	 
	    int h = 1;
	    while (h <= data.length / 3) {
	      h = h * 3 + 1;
	    }
	    while (h > 0) {
	      for (outer = h; outer < data.length; outer++) {
	        temp = data[outer];
	        inner = outer;
	 
	        while (inner > h - 1 && data[inner - h] >= temp) {
	          data[inner] = data[inner - h];
	          inner -= h;
	        }
	        data[inner] = temp;
	      }
	      h = (h - 1) / 3;
	    }
	    return data;
	}

	@Override
	public String getName() {
		return "ShellSort";
	}

}
