package at.feier.algorithms.sortalgorithms;

import java.util.LinkedList;

import at.feier.algorithms.helper.DataGenerator;

public class BallSorter implements SortAlgo{

	public int[] sort(int[] input) {
		LinkedList<Integer> result = new LinkedList<Integer>();
		//DataGenerator.printData(input);
		// Collections.sort(result);
		boolean first = false;
		int compare = 0;
		// ArrayList<Integer> result = new ArrayList<Integer>();
		for (int i : input) {
			if (first == false) {
				result.add(i);
				compare = i;
				first = true;
			} else if (first == true) {
				if (i == compare) {
					result.add(result.indexOf(compare), i);
					// System.out.println("Same");
				} else if (i > compare) {
					compare = i;
					result.addLast(i);
					// System.out.println("Bigger");
				} else if (i < compare) {
					// System.out.println("Smaller");
					int current = result.size();
					for (int p = 0; p < current; p++) {
						if (result.get(p) > i) {
							result.add(result.indexOf(result.get(p)), i);
							break;
						}
					}
				}
			}
		}
		int[] resultArray = new int[result.size()];
		for (int i = 0; i < resultArray.length; i++) {
			resultArray[i] = result.get(i);
		}
		return resultArray;
	}

	@Override
	public String getName() {
		return "BallSorter";
	}



}
