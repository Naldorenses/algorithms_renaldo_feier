package at.feier.algorithms.searchAlgorithms;

import java.util.ArrayList;
import java.util.Collections;

public class BST implements SearchAlgorithms {

	@Override
	public int Search(int[] a, int s) throws NumberNotFoundException {
		ArrayList<Integer> search = new ArrayList<Integer>();
		int pos = 0;
		int counter = 0;
		//boolean found = false;
		for(int i = 0; i < a.length;i++){
			search.add(a[i]);
		}
		Collections.sort(search);
		/*int mid = (search.size()-1)/2;
		while(found == false&&mid >1){
			int diffHigh = (search.size()-1)-mid;
			if(search.get(mid) == s){
				pos = mid;
				found = true;
			}
			else if(search.get(mid) < s){
				mid = mid / 2;
			}
			else if(search.get(mid) > s){
				mid = mid + (diffHigh/2);
			}
		}
		if(found == false){
			throw new NumberNotFoundException("Net do!");
		}*/
		while(search.size() != 1){
			int compare = search.get((search.size()-1)/2);
			if(compare > s){
				search.subList((search.size()-1)/2, search.size());
				counter++;
			}
			else if(compare < s){
				search.subList(0, (search.size()-1)/2);
				counter++;
			}
			else if(compare == s){
				pos = (int) (search.indexOf(compare)*Math.pow(2, counter));
				break;
			}
		}
		return pos;
		
	}

}
