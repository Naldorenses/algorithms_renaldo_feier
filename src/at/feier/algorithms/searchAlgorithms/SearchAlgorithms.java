package at.feier.algorithms.searchAlgorithms;


public interface SearchAlgorithms{

	public int Search(int[] a, int s) throws NumberNotFoundException;
	
}
