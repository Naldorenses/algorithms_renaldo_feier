package at.feier.algorithms.searchAlgorithms;

import java.util.ArrayList;

public class SequentialSearch implements SearchAlgorithms{
	public int Search(int[] searchAr, int searchFor) throws NumberNotFoundException {
		int counter = 0;
		ArrayList<Integer> pos = new ArrayList<Integer>();
		for (int i = 0; i < searchAr.length; i++) {
			if (searchAr[i] == searchFor) {
				pos.add(i);
				counter++;
			}
		}
		if (counter == 0) {
			System.out.println("Your number hasn't been found!");
		} else if (counter > 0) {
			System.out.println("Your number has been found " + counter + " times");
			System.out.print("at the position/s ");
			for (Integer o : pos) {
				if (pos.size() == 1) {
					System.out.print(o);
				} else {
					if (pos.indexOf(o) == pos.size() - 1) {
						System.out.print("and " + o + ".");
					} else {
						System.out.print(o + ", ");
					}
				}
			}
		}
		if(pos.size() == 0){
			throw new NumberNotFoundException("Nothing was found");
		}
		else{
			return pos.get(0);
		}
	}
}
