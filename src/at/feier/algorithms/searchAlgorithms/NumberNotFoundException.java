package at.feier.algorithms.searchAlgorithms;

public class NumberNotFoundException  extends Exception{
	public NumberNotFoundException(String message){
		super(message);
	}
}
