package at.feier.algorithms.helper;

public class DataGenerator {
	
	public static int[] generateRandomData(int Size, int MinValue, int MaxValue) {
		int[] numbers = new int[Size];
		for (int i = 0; i < Size; i++) {
			numbers[i] = (int) (Math.random() * (MaxValue - MinValue) + MinValue);
		}
		return numbers;

	}

	public static void printData(int[] data) {
		int size = data.length;
		System.out.println("Original Input");
		for (int i = 0; i < size; i++) {
			System.out.print(data[i] + " ");
		}
	}
	
	

}
