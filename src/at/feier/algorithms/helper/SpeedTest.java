package at.feier.algorithms.helper;

import java.util.ArrayList;

import at.feier.algorithms.sortalgorithms.SortAlgo;

public class SpeedTest {

	private ArrayList<SortAlgo> algorithms;

	public SpeedTest() {
		this.algorithms = new ArrayList<SortAlgo>();
	}

	public void addAlgorithm(SortAlgo sa) {
		algorithms.add(sa);
	}

	public void run() {
		int[] toSort = DataGenerator.generateRandomData(1000000, 1, 50);
		for (SortAlgo sa : algorithms) {
			int[] clone = toSort.clone();
			System.out.println(sa.getName());
			long startTime = System.nanoTime();
			sa.sort(clone);
			long endTime = System.nanoTime();
			long duration = (endTime - startTime) / 1000000;
			System.out.println(duration);
		}
	}

}
