package at.feier.recursion;

public class Recursion {
	public static void main(String[] args) {

		System.out.println(ggt(15, 10));
		System.out.println(addRec(323, 1));
		System.out.println(factorial(3));
		System.out.println(potenz(5, 3));
		printBinary(5);
		System.out.println();
		printHex(1302);
		System.out.println();
		System.out.println(fibonacci(8));
		System.out.println(isPalindrom("anna"));
	}

	private static int ggt(int a, int b) {
		if (a > b) {
			return ggt(a - b, b);
		} else if (a < b) {
			return ggt(a, b - a);
		} else {
			return a;
		}
	}

	private static int addRec(int a, int count) {
		if (count == 3) {
			return a;
		} else {
			return a + addRec(a, ++count);
		}
	}

	private static int factorial(int a) {
		if (a == 1) {
			return a;
		} else {
			return a * factorial(--a);
		}
	}

	private static int potenz(int x, int p) {
		if (p == 1) {
			return x;
		} else {
			return x * potenz(x, --p);
		}
	}

	private static void printBinary(int x) {
		if (x == 1) {
			System.out.print(x % 2);
		} else {
			printBinary(x / 2);
			System.out.print(x % 2);
		}
	}

	private static void printHex(int x) {
		if (x <= 16) {
			System.out.print(Integer.toHexString(x % 16));
		} else {
			printHex(x / 16);
			System.out.print(Integer.toHexString(x % 16));
		}
	}

	private static int fibonacci(int x) {
		if (x == 1 || x == 2) {
			return 1;
		} else {

			return fibonacci(x - 1) + fibonacci(x - 2);
		}
	}

	private static boolean isPalindrom(String str) {
		if (str.length() == 0 || str.length() == 1)
			return true;
		if (str.charAt(0) == str.charAt(str.length() - 1))
			return isPalindrom(str.substring(1, str.length() - 1));
		else {
			return false;
		}

	}

}
