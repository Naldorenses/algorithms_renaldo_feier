package algo;

public class Sum {

	public int sum(int a, int b) {
		String k = new StringBuilder("" + a).reverse().toString();
		String p = new StringBuilder("" + b).reverse().toString();
		char[] first = k.toCharArray();
		char[] second = p.toCharArray();
		int length = 0;
		char[] bigger;
		if (a < b) {
			length = second.length;
			bigger = second;
		} else {
			length = first.length;
			bigger = first;
		}
		String sum = "";
		int toAdd = 0;
		for (int i = 0; i < length; i++) {
			try {
				int tempsum = Integer.parseInt("" + first[i]) + Integer.parseInt("" + second[i]);
				if (toAdd != 0) {
					tempsum += toAdd;
					toAdd = 0;
				}
				if (tempsum < 10) {

					sum += tempsum;
				} else if (tempsum >= 10) {
					toAdd = tempsum / 10;
					char added = Integer.toString(tempsum).charAt(1);
					sum += added;
				}

			} catch (Exception e) {
				if (toAdd != 0) {
					int gottaAdd = Integer.parseInt("" + bigger[i]) + toAdd;
					if (gottaAdd >= 10) {
						toAdd = gottaAdd / 10;
						System.out.println(toAdd);
						char added = Integer.toString(gottaAdd).charAt(1);
						sum += added;
					} else {
						sum += Integer.parseInt("" + bigger[i]) + toAdd;
						toAdd = 0;
					}
				} else {
					sum += Integer.parseInt("" + bigger[i]);
				}
			}
		}
		sum += toAdd;
		String res = new StringBuilder(sum).reverse().toString();
		int resu = Integer.parseInt(res);
		return resu;

	}
}
