package algo;

public class Easter {

	public void easter(int j) {
		int n = j - 1900;
		int a = n % 19;
		int b = (7 * a + 1) / 19;
		int m = (11 * a + 4 - b) % 29;
		int q = (n / 4);
		int w = (n + q + 31 - m) % 7;
		int p = 25 - m - w;
		if (p > 0) {
			System.out.println("Ostersonntag ist der "+p+"te April!");
		}
		else if(p < 0){
			System.out.println("Ostersonntag ist der "+(p+31)+"te M�rz!");
		}
	}
}
