package algo;

public class Mult {

	public double mult(int a, int b) {
		String k = new StringBuilder("" + a).toString();
		String p = new StringBuilder("" + b).toString();
		char[] first = k.toCharArray();
		char[] second = p.toCharArray();
		int smaller;
		int length = 0;
		char[] biggerar;
		if (a < b) {
			length = second.length;
			biggerar = second;
			smaller = a;
		} else {
			length = first.length;
			biggerar = first;
			smaller = b;
		}
		int ten = (int) Math.pow(10, length - 1);
		int res = 0;
		for (int i = 0; i < length; i++) {
			int temp = Integer.parseInt("" + biggerar[i]) * smaller * ten;
			res += temp;
			ten /= 10;

		}
		return res;
	}

}
