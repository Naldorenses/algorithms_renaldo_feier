package algorithms;

import at.feier.algorithms.helper.DataGenerator;
import at.feier.algorithms.helper.SpeedTest;
import at.feier.algorithms.sortalgorithms.BallSorter;
import at.feier.algorithms.sortalgorithms.BubbleSort;
import at.feier.algorithms.sortalgorithms.InsertionSort;
import at.feier.algorithms.sortalgorithms.SelectionSort;
import at.feier.algorithms.sortalgorithms.SortAlgo;

public class MainClass {
	public static int min = 1;
	public static int max = 30;

	public static void main(String[] args) {

		int[] sort = DataGenerator.generateRandomData(50, min, max);
		SortAlgo sas = new SelectionSort();
		SortAlgo sab = new BubbleSort();
		SortAlgo sab1 = new BallSorter();
		SortAlgo sai = new InsertionSort();
		SortEngine se = new SortEngine();
		se.setSortAlgo(sas);
		se.printResult(se.sort(sort));
		SpeedTest st = new SpeedTest();
		/*st.addAlgorithm(sas);
		st.addAlgorithm(sai);
		//st.addAlgorithm(sab1);
		st.addAlgorithm(sab);
		st.run();*/

	}
}
